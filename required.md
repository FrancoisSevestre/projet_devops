# Required

## Vagrant
- Version: 2.2.6
- Installation: `sudo apt install vagrant`

## Virtualbox
- Version: 6.1.32
- Installation: `sudo apt install virtualbox`

## Java
- Version: 11
- Installation: `sudo apt-get install openjdk-11*`
- Configuration:
  - `sudo nano /etc/environment`
    - add `JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which javac)))))`
  - `sudo nano .bashrc`
    - add `JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which javac)))))`
  - `sudo reboot`

## Maven
- Version: 3.6.3
- Installation:
  ```
  wget https://downloads.apache.org/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz -P /tmp
  sudo tar xf /tmp/apache-maven-*.tar.gz -C /opt
  sudo ln -s /opt/apache-maven-3.6.3 /opt/maven
  sudo nano /etc/profile.d/maven.sh
  ```
    - Add:
    ```
    export JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which javac)))))
    export M2_HOME=/opt/maven
    export MAVEN_HOME=/opt/maven
    export PATH=${M2_HOME}/bin:${PATH}
    ```
  ```
  sudo chmod u+x,g+x,o+x /etc/profile.d/maven.sh
  sudo reboot
  ```

## Jenkins
- Version:
- Installation (Ubuntu):
  ```
  wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
  sudo nano /etc/apt/sources.list
  ```
    - Add `deb https://pkg.jenkins.io/debian-stable binary/`
  ```
  sudo apt-get update
  sudo apt-get install jenkins
  sudo ufw status
  sudo ufw disable
  ```

## Tomcat
- Version: 8.5.79
- Installation:
  ```
  wget https://downloads.apache.org/tomcat/tomcat-8/v8.5.79/bin/apache-tomcat-8.5.79.tar.gz
  sudo tar xvfz apache-tomcat-8.5.79.tar.gz -C /opt
  sudo mv /opt/apache-tomcat-8.5.79/ /opt/tomcat8
  sudo chmod +x /opt/tomcat8/bin/startup.sh
  sudo chmod +x /opt/tomcat8/bin/shutdown.sh
  sudo ln -s /opt/tomcat8/bin/startup.sh /usr/local/bin/tomcatup
  sudo ln -s /opt/tomcat8/bin/shutdown.sh /usr/local/bin/tomcatdown
  ```
- Configuration:
  ```
  sudo nano /opt/tomcat8/webapps/manager/META-INF/context.xml
  ```
    - Comment: 
    ```
    <!--Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" /-->
    ```
  ```
  sudo nano /opt/tomcat8/webapps/host-manager/META-INF/context.xml
  ```
    - comment:
    ```
    <!--Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" /-->
    ```
  ```
  sudo nano /opt/tomcat8/conf/tomcat-users.xml
  ```
    - Add: 
    ```
    <role rolename="manager-gui"/> 
    <role rolename="manager-script"/> 
    <role rolename="manager-jmx"/> 
    <role rolename="manager-status"/> 
    <user username="admin" password="admin" roles="manager-gui, manager-script, manager-jmx, manager-status"/> 
    <user username="deployer" password="deployer" roles="manager-script"/> 
    <user username="tomcat" password="s3cret" roles="manager-gui"/>
    ```
  ```
  $ sudo /usr/local/bin/tomcatup
  ```

## Docker
- Version: 20.10.17
- Installation:
  ```
  sudo apt install apt-transport-https ca-certificates curl software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
  sudo apt update
  sudo apt install docker-ce
  systemctl status docker
  ```
  - S’il n’est pas démarré : `sudo systemctl start docker`
- Configuration:
  - Pour démarrer Docker au démarrage de la machine : `sudo systemctl enable docker`
  - Donner les accès de l'utilisateur au groupe Docker:
    - `$ sudo usermod -aG docker vagrant`
    - `$ newgrp docker`
    - `$ sudo usermod -aG docker vagrant`

## Docker-compose : NON UTILISE ICI
- Version: 1.17.1
- Installation: `$ sudo apt install docker-compose`

## SonarQube
- Version: 5.4
- Installation:
  ```
  sudo apt install unzip
  wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-8.9.8.54436.zip
  unzip sonarqube-8.9.8.54436.zip
  sudo mv sonarqube-8.9.8.54436 /opt/sonarqube-8
  /opt/sonarqube-8/bin/linux-x86-64/sonar.sh start
  /opt/sonarqube-8/bin/linux-x86-64/sonar.sh status
  ```
- Configuration: Se connecter à SonarQube (Port 9005)
  - Id de connexion : `admin`/`admin` -> changer le mot de passe : `formation`
  - Ajouter dans pom.xml (repo GitHub) :
    ```
    <plugin>
      <groupId>org.sonarsource.scanner.maven</groupId>
      <artifactId>sonar-maven-plugin</artifactId>
      <version>3.0</version>
    </plugin>
    <plugin>
      <groupId>org.jacoco</groupId>
      <artifactId>jacoco-maven-plugin</artifactId>
      <version>0.8.6</version>
    </plugin>
    ```

## SonarScanner
- Version: 4.5.0
- Installation: 
  ```
  wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.5.0.2216-linux.zip
  unzip sonar-scanner-cli-4.5.0.2216-linux.zip
  sudo mv sonar-scanner-4.5.0.2216-linux/ /opt/sonar-scanner-4.5
  ```

## Artifactory : NON UTILISE ICI
TODO?

## Ansible
- Version: 
- Installation: 

## NodeExporter
- Version: 0.18.1
- Installation:
  ```
  sudo useradd -rs /bin/false node_exporter
  wget https://github.com/prometheus/node_exporter/releases/download/v0.18.1/node_exporter-0.18.1.linux-amd64.tar.gz
  tar -xvzf node_exporter-0.18.1.linux-amd64.tar.gz
  sudo mv node_exporter-0.18.1.linux-amd64/node_exporter /usr/local/bin/
  sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
  ```
- Configuration:
  ```
  sudo vim /etc/systemd/system/node_exporter.service
  ```
  - Add:
    ```
    [Unit]
    Description=Node Exporter
    After=network-online.target
    [Service]
    User=node_exporter
    Group=node_exporter
    Type=simple
    ExecStart=/usr/local/bin/node_exporter
    [Install]
    WantedBy=multi-user.target
    ```
  ```
  sudo systemctl daemon-reload
  sudo systemctl enable --now node_exporter
  curl localhost:9100
  curl localhost:9100/metrics
  ```

## Cluster Kubernetes
### Master + Workers

```
sudo apt-get install curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
sudo apt-get install -y kubelet=1.21.3-00 kubeadm=1.21.3-00 kubectl=1.21.3-00
sudo systemctl enable --now kubelet
kubeadm version
```
`sudo hostnamectl set-hostname xxxnode`

```
echo "
192.168.60.40 masternode
192.168.60.50 workernode
" | sudo tee -a /etc/hosts
```

```
sudo ufw allow 6443/tcp
sudo ufw allow 2379:2380/tcp
sudo ufw allow 10248/tcp
sudo ufw allow 10250/tcp
sudo ufw allow 10251/tcp
sudo ufw allow 10252/tcp
sudo ufw allow 10255/tcp
sudo ufw allow 6783/tcp
sudo ufw allow 6783/udp
sudo ufw allow 6784/udp
sudo cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
sudo swapoff -a
```


### Master
`sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=192.168.60.40`
```
mkdir -p $HOME/.kube
vagrant@masternode:~$   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
vagrant@masternode:~$   sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
`echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> .bashrc`
Noter les tokens
`wget https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml`

Modifier le fichier yaml: ajouter --iface=enp0s8 apres la ligne 202 (`ip a` pour le nom du réseau)

### workers
```
sudo kubeadm join 192.168.60.40:6443 --token xxxx --discovery-token-ca-cert-hash sha256:xxxx

```
