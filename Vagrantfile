#  -*-  mode:  ruby -*-
# vi: set ft=ruby  :

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION)  do  |config|
  # General Vagrant VM   configuration.
  config.vm.box  =  "ubuntu/bionic64"
  config.vm.synced_folder  ".",  "/vagrant"
  config.vm.provision "shell" do |s|
  ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
  s.inline = <<-SHELL
    echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys
    echo #{ssh_pub_key} >> /root/.ssh/authorized_keys
  SHELL
  end

  #  ControlMaster
  config.vm.define  "jenkins"  do  |app|
    app.vm.hostname  =  "jenkins"
    app.vm.network  :private_network,  ip:  "192.168.60.10"
    app.vm.provision :shell, :inline => "sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config; sudo systemctl restart sshd;", run: "always"
    # Jenkins
    app.vm.network "forwarded_port", guest: 8080, host: 9000
    # SonarQube
    app.vm.network "forwarded_port", guest: 9000, host: 9005
    # Node_exporter
    app.vm.network "forwarded_port", guest: 9100, host: 9015
    app.vm.provider  :virtualbox  do  |v|
    	v.memory  =  6000
      v.cpus = "2"
    end
  end

	#  Application  server 
  config.vm.define  "app"  do  |app|
  	app.vm.hostname  =  "app"
  	app.vm.network  :private_network,  ip:  "192.168.60.20"
    app.vm.provision :shell, :inline => "sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config; sudo systemctl restart sshd;", run: "always"
    # Petclinic in Tomcat
    app.vm.network "forwarded_port", guest: 8080, host: 9001
    # Petclinic in Docker
    app.vm.network "forwarded_port", guest: 8081, host: 9004
    # Node_exporter
    app.vm.network "forwarded_port", guest: 9100, host: 9014
	app.vm.provider  :virtualbox  do  |v|
    	v.memory  =  4096
    end
  end

	#  Database  server.
  config.vm.define  "supervision"  do  |app|
  	app.vm.hostname  =  "supervision"
  	app.vm.network  :private_network,  ip:  "192.168.60.30"
    app.vm.provision :shell, :inline => "sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config; sudo systemctl restart sshd;", run: "always"
    # MySQL
    app.vm.network "forwarded_port", guest: 3306, host: 9003
    # Grafana
    app.vm.network "forwarded_port", guest: 3000, host: 9012
    # Prometheus
    app.vm.network "forwarded_port", guest: 9090, host: 9011
    # Node_exporter
    app.vm.network "forwarded_port", guest: 9100, host: 9016
	app.vm.provider  :virtualbox  do  |v|
    	v.memory  =  2048
    end
  end

	#  Masternode kubernetes
  config.vm.define  "masternode"  do  |app|
  	app.vm.hostname  =  "masternode"
  	app.vm.network  :private_network,  ip:  "192.168.60.40"
    app.vm.provision :shell, :inline => "sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config; sudo systemctl restart sshd;", run: "always"
    # Node_exporter
    app.vm.network "forwarded_port", guest: 9100, host: 9017
	app.vm.provider  :virtualbox  do  |v|
    	v.memory  =  4096
      v.cpus = "2"
    end
  end

	#  Worker kubernetes
  config.vm.define  "workernode1"  do  |app|
  	app.vm.hostname  =  "workernode1"
  	app.vm.network  :private_network,  ip:  "192.168.60.50"
    app.vm.provision :shell, :inline => "sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config; sudo systemctl restart sshd;", run: "always"
    # Node_exporter
    app.vm.network "forwarded_port", guest: 9100, host: 9018
    app.vm.network "forwarded_port", guest: 30080, host: 9008
	app.vm.provider  :virtualbox  do  |v|
    	v.memory  =  2048
    end
  end

	#  Worker kubernetes
  config.vm.define  "workernode2"  do  |app|
  	app.vm.hostname  =  "workernode2"
  	app.vm.network  :private_network,  ip:  "192.168.60.60"
    app.vm.provision :shell, :inline => "sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config; sudo systemctl restart sshd;", run: "always"
    # Node_exporter
    app.vm.network "forwarded_port", guest: 9100, host: 9019
    app.vm.network "forwarded_port", guest: 30080, host: 9009
	app.vm.provider  :virtualbox  do  |v|
    	v.memory  =  2048
    end
  end

  # Run Ansible Playbook
#  config.vm.provision "ansible" do |ansible|
#    ansible.playbook="ressources/ansible/start.yml"
#  end
end
